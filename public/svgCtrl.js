var text = '{ "dataList" : [' +
'{ "ipDate":"2018-03-01" , "ipTime":"01:00" , "val0":"30" , "val2":"20" , "val3":"20" ,"val4":"40" , "val6":"10" ,  "val7":"50" , "val8":"10" ,"val9":"80" , "val10":"20" },' +
'{ "ipDate":"2018-03-01" , "ipTime":"08:00" , "val0":"10" , "val2":"50" , "val3":"10" ,"val4":"10" , "val6":"20" ,  "val7":"50" , "val8":"30" ,"val9":"60" , "val10":"40" },' +
'{ "ipDate":"2018-03-01" , "ipTime":"12:00" , "val0":"40" , "val2":"30" , "val3":"40" ,"val4":"20" , "val6":"40" ,  "val7":"60" , "val8":"20" ,"val9":"50" , "val10":"60" },' +
'{ "ipDate":"2018-03-01" , "ipTime":"13:00" , "val0":"20" , "val2":"40" , "val3":"70" ,"val4":"30" , "val6":"30" ,  "val7":"40" , "val8":"10" ,"val9":"70" , "val10":"80" },' +
'{ "ipDate":"2018-03-01" , "ipTime":"17:00" , "val0":"30" , "val2":"10" , "val3":"50" ,"val4":"10" , "val6":"50" ,  "val7":"70" , "val8":"30" ,"val9":"40" , "val10":"70" },' +
'{ "ipDate":"2018-03-01" , "ipTime":"10:00" , "val0":"60" , "val2":"30" , "val3":"60" ,"val4":"30" , "val6":"60" ,  "val7":"90" , "val8":"60" ,"val9":"30" , "val10":"50" } ]}'; 

var obj = JSON.parse(text); 

console.log("sdf");
var createCircle = d3.selectAll(".signal");//.select("line");
var count = 0;
createCircle.each( function(d){
	var x1 =d3.select(this).select("line").attr("x1");
	var x2 =d3.select(this).select("line").attr("x2");
	var y1 =d3.select(this).select("line").attr("y1");
	var y2 =d3.select(this).select("line").attr("y2");
	var xdiff;
	if(Number(x1)>Number(x2))
		xdiff = 20;
	else
		xdiff = -20;
	var y;
	if(562-Number(y2)<60)
		y = Number(y2)-20;
	else 
		y = Number(y2)+20; 
	var x = Number(x2)+xdiff;
	x = x.toString();
	y = y.toString();
	if(count != 1 && count != 5){
		var g= d3.select(this).append("g").attr("id","val"+count);
		g.append("ellipse").attr("cx",x).attr("cy",y).attr("rx","19").attr("ry","15").attr("fill","#eda20b");
		//g.append("text").attr("x",x).attr("y",y).attr("text-anchor","middle").attr("stroke","#000000").style("font-size","12px").text("10");

	}
	count++;	
});
//fillText(1,0);

function color(ip, op){
	if(ip<op)
		return "green";
	else if(ip>op)
		return "red";
	else
		return "yellow";

}

function changeColor(col, id){
	var sel = d3.selectAll(id).each( function(d){
		d3.select(this).attr("stroke",col);

	});

}

function fillText(index,val){
	var g= d3.select("#val"+val);
	g.select("text").remove();
	var ell = g.select("ellipse");
	var x =ell.attr("cx");
	var y =ell.attr("cy");
	var t;
	switch(val){
		case 0:
			t = obj.dataList[index].val0;
			break;
		case 2:
			t = obj.dataList[index].val2;
			break;
		case 3:
			t = obj.dataList[index].val3;
			break;
		case 4:
			t = obj.dataList[index].val4;
			break;
		case 6:
			t = obj.dataList[index].val6;
			break;
		case 7:
			t = obj.dataList[index].val7;
			break;
		case 8:
			t = obj.dataList[index].val8;
			break;
		case 9:
			t = obj.dataList[index].val9;
			break;
		case 10:
			t = obj.dataList[index].val10;
			break;
	}
	console.log(t);
	g.append("text").attr("x",x).attr("y",y).attr("text-anchor","middle").attr("stroke","#000000").style("font-size","16px").text(t);

}

var btn = document.getElementById("check").addEventListener("click", function(){
	var ip = document.getElementById("ip1").value;
	console.log(ip);
	var ip2 = document.getElementById("hour").value+":"+document.getElementById("min").value;
	console.log(ip2);
	for(var i=0 ;i <6;i++){
		if(obj.dataList[i].ipDate == ip && obj.dataList[i].ipTime == ip2)
		{
			for(var j=0; j<=10;j++){
				if(j != 1 && j != 5){
					fillText(i,j);
				}
			}

			// check for reactor
			col = color((Number(obj.dataList[i].val0)+Number(obj.dataList[i].val4)+Number(obj.dataList[i].val9)),Number(obj.dataList[i].val2));
			changeColor(col, "#reactor");
			// check for ccoling coil
			//nothing needs to be done as there is no input or output paths
			// check for condensor1
			col = color(Number(obj.dataList[i].val2),Number(obj.dataList[i].val3));
			changeColor(col, "#condensor1");
			// check for Vapour_Liquid_Seperator
			col = color((Number(obj.dataList[i].val3)),(Number(obj.dataList[i].val4)+Number(obj.dataList[i].val6)));
			changeColor(col, "#seperator");
			// check for Heater
			//nothing needs to be done as there is no input or output paths
			// check for Stripper
			col = color((Number(obj.dataList[i].val6)+Number(obj.dataList[i].val7)),(Number(obj.dataList[i].val8)+Number(obj.dataList[i].val9)+Number(obj.dataList[i].val10)));
			changeColor(col, "#stripper");
			// check for Product_Outlet
			col = color(Number(obj.dataList[i].val8),0);
			changeColor(col, "#outlet");
			// check for purge
			col = color(Number(obj.dataList[i].val10),0);
			changeColor(col, "#purge");
			break;
			}
		}

});